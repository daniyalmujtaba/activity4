package com.example.datasharing_activity4;


import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayActivity extends AppCompatActivity {

    private TextView fname, lname, rollno;
    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        fname = findViewById(R.id.firstname);
        lname = findViewById(R.id.lastname);
        rollno = findViewById(R.id.rollno);
        back = findViewById(R.id.back);
        Intent details = getIntent();
        String firstname = details.getStringExtra("firstname");
        String lastname = details.getStringExtra("lastname");
        String rno = details.getStringExtra("rollno");
        fname.setText(firstname);
        lname.setText(lastname);
        rollno.setText(rno);
    }
}