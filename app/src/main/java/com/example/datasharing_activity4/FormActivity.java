package com.example.datasharing_activity4;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class FormActivity extends AppCompatActivity {

    private EditText firstname,lastname,rollno;
    private Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        firstname=findViewById(R.id.firstname);
        lastname=findViewById(R.id.lastname);
        rollno=findViewById(R.id.rollno);
        submit=findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fname=firstname.getText().toString();
                String lname=lastname.getText().toString();
                String rno=rollno.getText().toString();


                Intent intent=new Intent(FormActivity.this,DisplayActivity.class);
                intent.putExtra("firstname",fname);
                intent.putExtra("lastname",lname);
                intent.putExtra("rollno",rno);
                startActivity(intent);



            }
        });
    }
}